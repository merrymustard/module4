-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 01, 2018 at 03:05 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `sharkstatus`
--

-- --------------------------------------------------------

--
-- Table structure for table `nameSharks`
--

CREATE TABLE `nameSharks` (
  `idname` int(11) NOT NULL,
  `nameshark` varchar(50) NOT NULL,
  `statusShark` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nameSharks`
--

INSERT INTO `nameSharks` (`idname`, `nameshark`, `statusShark`) VALUES
(1, 'white shark', '1'),
(2, 'endangered', '0'),
(3, 'hamme', 'endangered'),
(4, 'aa', 'ii'),
(5, 'abe', '123'),
(6, 'a', 'a'),
(7, 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `statusShark`
--

CREATE TABLE `statusShark` (
  `IdStatus` int(11) NOT NULL,
  `namestatus` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `nameSharks`
--
ALTER TABLE `nameSharks`
  ADD PRIMARY KEY (`idname`);

--
-- Indexes for table `statusShark`
--
ALTER TABLE `statusShark`
  ADD PRIMARY KEY (`IdStatus`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nameSharks`
--
ALTER TABLE `nameSharks`
  MODIFY `idname` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `statusShark`
--
ALTER TABLE `statusShark`
  MODIFY `IdStatus` int(11) NOT NULL AUTO_INCREMENT;