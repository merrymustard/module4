package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;

public class Shark {
	
	//attributes
	private int idname;
	private String sharkName;
	private String sharkStatus;

	//db connection
	private Connection conn = null;
	private Statement stmt = null;
	
	
	// constructor
	public Shark() {
		try {
			// register driver
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			
			//connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost:8889/sharkstatus?user=root&password=root");
			
			//create statement
			this.stmt = this.conn.createStatement();
					
		}catch(Exception ex) {
			System.out.println("error on User contrsuction:" + ex.getMessage());
		}
	}

	
	
	//encapsulation
	public int getidname() {
		return idname;
	}


	public void setidname(int idname) {
		this.idname = idname;
	}


	public String getSharkName() {
		return sharkName;
	}


	public void setSharkName(String sharkName) {
		this.sharkName = sharkName;
	}


	public String getSharkStatus() {
		return sharkStatus;
	}


	public void setSharkStatus(String sharkStatus) {
		this.sharkStatus = sharkStatus;
	}

	
   //crud methods
	public boolean save(String sharkName, String sharkStatus) {
		try {
			
			//SQL String
			String sql = "INSERT INTO nameSharks (idname,nameshark,statusShark) VALUES (NULL, '" + sharkName + "','" + sharkStatus +"')";
			//execute SQL String
			
			System.out.println(sql);
			//Execute query
			this.stmt.executeUpdate(sql);
			

			return true;
		}catch (Exception ex){
			System.out.println("error on save shark " + ex.getMessage());
			return false;
		}
	}
	public boolean delete (int idname) {
		return true;
		
	}
	public  ArrayList<Shark> getSharks() {
		return null;
	}
	
}
