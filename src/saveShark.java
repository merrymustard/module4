

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import model.Shark;

@WebServlet("/saveShark")
public class saveShark extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public saveShark() {
        super();
    }


	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//start
		//PERMISSIONS
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		//PARAMETERS
		String sharkName = request.getParameter("sharkName");
		String sharkStatus = request.getParameter("sharkStatus");
		
		//CREATE OBJECT s with the name class
		Shark s = new Shark();
		//create a method now
		
		boolean issaved = s.save(sharkName,sharkStatus);
		
		JSONArray list = new JSONArray();
		JSONObject item = new JSONObject();
		
		
		if (issaved) {
			item.put("connect", true);
		}else {
			item.put("connect", false);
		}
		list.add(item);
		
		
		response.getWriter().append(list.toJSONString());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
